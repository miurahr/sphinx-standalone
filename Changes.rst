==========
ChangeLog
==========

All notable changes to this project will be documented in this file.

`Unreleased`_
=============

v1.0.0_
=======

* Move tools into the package
* Support launching quickstart with subcommand
* Supported subcommands are
  - apidoc
  - autogen
  - build
  - quickstart
  - intl
* show help when unkown subcommand given

v0.9.1_
=======

* Fix sphinx_rtd_theme not found error.
* Drop monkey patches.
* Add sphinxcontrib extensions; ditaa, drawio, svgbob, and xlsxtable


v0.9.0
======

* First public release.

.. _Unreleased: https://codeberg.org/miurahr/sphinx-standalone/compare/v1.0.0...HEAD
.. _v1.0.0: https://codeberg.org/miurahr/sphinx-standalone/compare/v0.9.1...v1.0.0
.. _v0.9.1: https://codeberg.org/miurahr/sphinx-standalone/compare/v0.9.0...v0.9.1
