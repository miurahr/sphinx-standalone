Sphinxcontrib-ditaa
===================

.. ditaa::

    +--------+   +-------+    +-------+
    |        | --+ ditaa +--> |       |
    |  Text  |   +-------+    |diagram|
    |Document|   |!magic!|    |       |
    |     {d}|   |       |    |       |
    +---+----+   +-------+    +-------+
        :                         ^
        |       Lots of work      |
        +-------------------------+


.. ditaa::
   :--no-antialias:
   :--transparent:
   :--scale: 1.5
   :alt: a test for ditaa.
   :width: 600
   :height: 400
   :align: left
   :scale: 50

    Color codes
    /-------------+-------------\
    |cRED RED     |cBLU BLU     |
    +-------------+-------------+
    |cGRE GRE     |cPNK PNK     |
    +-------------+-------------+
    |cBLK BLK     |cYEL YEL     |
    \-------------+-------------/