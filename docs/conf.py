# -*- coding: utf-8 -*-
import alabaster
import sphinx_bootstrap_theme
import sphinx_rtd_theme
import yaml

# Ensure we can load a YAML file.
with open('test.yaml', 'r') as stream:
    test_yaml = yaml.load(stream, Loader=yaml.FullLoader)
    assert test_yaml[0] == 'a'
    assert test_yaml[1] == 'b'
    assert test_yaml[2] == 'c'

project = u'sphinx-standalone-demo'
copyright = '2022, Hiroshi Miura;2018, Trustin Lee'
version = '2.0'
release = '2.0.0'

# General options
needs_sphinx = '1.0'
master_doc = 'index'
pygments_style = 'tango'
add_function_parentheses = True

extensions = ['myst_parser', 'sphinx.ext.autodoc', 'sphinx.ext.autosectionlabel',
              'sphinx.ext.autosummary', 'sphinx.ext.coverage', 'sphinx.ext.doctest',
              'sphinx.ext.extlinks', 'sphinx.ext.githubpages', 'sphinx.ext.graphviz',
              'sphinx.ext.ifconfig', 'sphinx.ext.imgconverter', 'sphinx.ext.inheritance_diagram',
              'sphinx.ext.intersphinx', 'sphinx.ext.linkcode', 'sphinx.ext.imgmath',
              'sphinx.ext.napoleon', 'sphinx.ext.todo', 'sphinx.ext.viewcode',
              'sphinxcontrib.drawio', "sphinxcontrib.xlsxtable",
              'sphinxcontrib.ditaa', 'sphinxcontrib.httpdomain', 'sphinxcontrib.imagesvg',
              'sphinxcontrib.openapi', 'sphinxcontrib.plantuml', 'sphinxcontrib.redoc',
              'sphinxcontrib.svgbob', 'sphinxcontrib.youtube', 'sphinxemoji.sphinxemoji']

templates_path = ['_templates']
exclude_trees = ['_build']

# HTML options
html_theme = 'bootstrap'
html_theme_path = sphinx_bootstrap_theme.get_html_theme_path()
print(alabaster.get_path())
print(sphinx_bootstrap_theme.get_html_theme_path())
print(sphinx_rtd_theme.get_html_theme_path())

html_short_title = "sphinx-gradle"
htmlhelp_basename = 'sphinx-gradle-doc'
html_use_index = True
html_show_sourcelink = False
html_static_path = ['_static']

myst_enable_extensions = [
    "amsmath",
    "colon_fence",
    "deflist",
    "dollarmath",
    "fieldlist",
    "html_admonition",
    "html_image",
    "linkify",
    "replacements",
    "smartquotes",
    "strikethrough",
    "substitution",
    "tasklist",
]

# PlantUML options
plantuml = "java -jar _extentions/plantuml-1.2021.1.jar"

# ReDoc options
redoc = [{ 'name': 'Batcomputer API',
           'page': 'sphinxcontrib-redoc',
           'spec': 'openapi.yaml' }]

# SphinxEmoji options
sphinxemoji_style = 'twemoji'

# linkcode options
def linkcode_resolve(domain, info):
    return "https://example.com/linkcode.html"

ditaa = "java"
ditaa_args = ["-jar", "_extensions/ditaa0_11-standalone.jar"]
ditaa_log_enable = True
