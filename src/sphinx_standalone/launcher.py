#!python.exe
import sys

from sphinx.cmd.build import main as sphinx_build

from typing import List, Tuple

__description__ = """Sphinx-standalone - standalone sphinx binary.
Copyright (C) 2022,2023 Hiroshi Miura

Sphinx-standalone is a single binary utility to run sphinx-build.
"""

def main():
    sphinx_build()


if __name__ == '__main__':
    sys.exit(main())
