submodules = [
    "sphinx",
    "sphinxcontrib",
    "sphinxemoji",
    "markdown-it",
    "linkify-it",
    "babel",
    "docutils",
    "openpyxl",
    "myst_parser",
    "PIL",
    # themes
    "alabaster",
    "sphinx_bootstrap_theme",
    "sphinx_rtd_theme"
]
hiddenimports = [
    "babel",
    "click",
    "inspect",
    'jinja2',
    "locale",
    "lib2to3",
    "pkg_resources",
    "pytz",
    "yaml",
    'sphinx_material'
]
collect_all = [
    'certifi',
]
metadata = [
    'charset_normalizer',
]
package_data = [
    'babel',
    'docutils',
    'jsonschema',
    'pytz',
    'markdown_it',
    'sphinxemoji',
    'alabaster',
    'sphinx_bootstrap_theme',
    'sphinx_rtd_theme',
    'sphinx_material',
]
excludes = [
    'numpy',
    'toml',
    'setuptools',
    'distutils',
    'pygls',
    'matplotlib',
    'pyparsing',
]