import sys


if __name__ == "__main__":
    from . import builder
    sys.exit(builder.main())
