import argparse
import hashlib
import importlib.machinery
import importlib.metadata
import importlib.util
import os
import pathlib
import sys
from typing import Any, Optional

import PIL
import sphinxcontrib

from . import config

PROJECT_NAME="sphinx-standalone"
DIST_DIR = pathlib.Path("dist")
LAUNCHER = "launcher.py"

def is_64bit() -> bool:
    """check if running platform is 64bit python."""
    return sys.maxsize > 1 << 32


def build(name: str, sha: bool, loglevel: str) -> int:
    # PyInstaller arguments
    pyinstaller_args = [
        "--noconfirm",
        "--console",
        "--onefile",
        "--name", name
    ]

    add_arg = "{src:s}" + os.pathsep + "{name:s}"
    for mod in config.collect_all:
        pyinstaller_args.append("--collect-all")
        pyinstaller_args.append(mod)
    for mod in config.package_data:
        origin = importlib.util.find_spec(mod).origin
        if origin is not None:
            pyinstaller_args.append("--add-data")
            pyinstaller_args.append(add_arg.format(src=os.path.dirname(origin), name=mod))
    for mod in config.hiddenimports:
        pyinstaller_args.append("--hidden-import")
        pyinstaller_args.append(mod)
    for sub in config.submodules:
        pyinstaller_args.append("--collect-submodules")
        pyinstaller_args.append(sub)
    for pkg in config.metadata:
        pyinstaller_args.append("--copy-metadata")
        pyinstaller_args.append(pkg)
    for mod in config.excludes:
        pyinstaller_args.append("--exclude-module")
        pyinstaller_args.append(mod)

    # hack for sphinxcontrib data
    pyinstaller_args.append("--add-data")
    pyinstaller_args.append(add_arg.format(src=sphinxcontrib.__path__[0], name="sphinxcontrib"))

    # work around for "WARNING: Cannot find lib*.so.* (needed by ...)"
    if sys.platform.startswith("linux"):
        libs = str(pathlib.Path(os.path.dirname(PIL.__path__[0])).joinpath("Pillow.libs").resolve())
        if "LD_LIBRARY_PATH" in os.environ:
            lp = os.getenv("LD_LIBRARY_PATH")
            os.environ["LD_LIBRARY_PATH"] = "{0}{1}{2}".format(libs, os.pathsep, lp)
        else:
            os.environ["LD_LIBRARY_PATH"] = libs

    pyinstaller_args.append("--log-level")
    pyinstaller_args.append(loglevel)
    # give launcher script
    pyinstaller_args.append(os.path.join(os.path.dirname(__file__), LAUNCHER))
    # launch PyInstaller
    from PyInstaller import __main__ as pyinstaller
    pyinstaller.run(pyinstaller_args)
    #
    if sys.platform.startswith("win"):
        # .exe is automatically added on windows
        genfile = name + ".exe"
    else:
        genfile = name
    # Generate sha
    if sha:
        gen_sha(genfile)
    return 0


def gen_sha(file: str):
    _gen_sha(file, "sha256")
    _gen_sha(file, "sha512")


def _gen_sha(file: str, type: str):
    hash = hashlib.new(type)
    generated = DIST_DIR / file
    with generated.open(mode="rb") as bin:
        hash.update(bin.read())
    hashfile = DIST_DIR / f"{file}.{type}"
    with hashfile.open(mode="w") as f:
        f.write(hash.hexdigest())


def get_name():
    if sys.platform.startswith("win"):
        if is_64bit():
            return "sphinx.windows-x86_64"
        else:
            return "sphinx.windows-x86"
    elif sys.platform.startswith("linux"):
        return "sphinx.linux"
    elif sys.platform.startswith("darwin"):
        return "sphinx.osx"
    else:
        return "sphinx"


def main(arg: Optional[Any] = None) -> int:
    parse = argparse.ArgumentParser(prog="mkdocs-standalone", description="Standalone mkdocs binary builder")
    parse.add_argument("-n", "--name", nargs=1, help="Specify generated binary name.")
    parse.add_argument("-s", "--sha", action="store_true", help="Generate SHA256 and SHA512 hash files.")
    parse.add_argument("--log-level", nargs=1, help="Specify log level, WARN, INFO, or DEBUG", default="INFO")
    args = parse.parse_args(arg)
    #
    if args.name is not None:
        name = args.name[0]
    else:
        name = get_name()
    return(build(name, sha=args.sha, loglevel=args.log_level))


if __name__ == "__main__":
    sys.exit(main(None))
