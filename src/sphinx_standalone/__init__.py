import sys


__copyright__ = "Copyright (C) 2022,2023 Hiroshi Miura"


# Launch Sphinx
if __name__ == "__main__":
    from . import launcher
    sys.exit(launcher.main())
