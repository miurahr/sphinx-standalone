Sphinx standalone
=================

Run Sphinx_ on Windows, Linux and OS X with a single-file standalone binary.
Built with PyInstaller_ , a user can run Sphinx without installing a Python interpreter
or any dependencies.

How to run
----------

Download standalone binary from release and save it in arbitrary name in a place
in PATH environment variable.
Here we assume you saved  it as name ``sphinx.linux``

When you want to build html document,

.. code-block::

   $ sphinx.linux ./src ./_build


Additional modules
------------------

As well as vanilla Sphinx, the following modules are provided out of the box
for your convenience:

- PyYAML_
- MyST-parser_
- sphinxcontrib-ditaa_
- sphinxcontrib-drawio_
- sphinxcontrib-httpdomain_
- sphinxcontrib-imagesvg_
- sphinxcontrib-openapi_
- sphinxcontrib-redoc_
- sphinxcontrib-svgbob_
- sphinxcontrib-xlsxtable_
- sphinxcontrib-websupport_
- sphinxcontrib-youtube_
- sphinxemoji_
- sphinx-rtd-theme_
- sphinx-bootstrap-theme_
- sphinx-material

Acknowledgement
---------------

Sphinx-standalone project is born with inspiration from the `sphinx-binary`_ project.

License
-------

Sphinx-standalone is licensed under the 2-clauses BSD license as same as the Sphinx project.
See LICENSE for details.


.. _Sphinx: http://www.sphinx-doc.org/
.. _PyInstaller: https://www.pyinstaller.org/
.. _PyYAML: https://pypi.org/project/PyYAML/
.. _MyST-parser: https://pypi.org/project/myst-parser/

.. _sphinxcontrib-ditaa: https://pypi.org/project/sphinxcontrib-ditaa/
.. _sphinxcontrib-drawio: https://pypi.org/project/sphinxcontrib-drawio/
.. _sphinxcontrib-httpdomain: https://pypi.org/project/sphinxcontrib-httpdomain/
.. _sphinxcontrib-imagesvg: https://pypi.org/project/sphinxcontrib-imagesvg/
.. _sphinxcontrib-openapi: https://pypi.org/project/sphinxcontrib-openapi/
.. _sphinxcontrib-redoc: https://pypi.org/project/sphinxcontrib-redoc/
.. _sphinxcontrib-svgbob: https://pypi.org/project/sphinxcontrib-svgbob/
.. _sphinxcontrib-xlsxtable: https://pypi.org/project/sphinxcontrib-xlsxtable/
.. _sphinxcontrib-websupport: https://pypi.org/project/sphinxcontrib-websupport/
.. _sphinxcontrib-youtube: https://pypi.org/project/sphinxcontrib-youtube/
.. _sphinxemoji: https://pypi.org/project/sphinxemoji/
.. _sphinx-rtd-theme: https://pypi.org/project/sphinx-rtd-theme/
.. _sphinx-bootstrap-theme: https://pypi.org/project/sphinx-bootstrap-theme/

.. _sphinx-binary: https://github.com/trustin/sphinx-binary
